package com.franciscocalaca.agenda;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/contato")
public class ContatoRest {

	@Autowired
	private ContatoDao contatoDao;
	
	@GetMapping
	public List<Contato> get(){
		return contatoDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Contato get(@PathVariable("id") Integer id) {
		return contatoDao.findById(id).get();
	}
	
	@PostMapping
	public void post(@RequestBody Contato contato) {
		contatoDao.save(contato);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Integer id) {
		contatoDao.deleteById(id);
	}
	
	@PutMapping("/{id}")
	public Contato put(@PathVariable("id") Integer id, @RequestBody Contato contato) {
		Contato ct = contatoDao.findById(id).get();
		ct.setEndereco(contato.getEndereco());
		ct.setNome(contato.getNome());
		ct.setTelefone(contato.getTelefone());
		contatoDao.save(ct);
		return ct;
	}
	
}
