package com.franciscocalaca.agenda;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.http.utils.Token;

@RestController
@RequestMapping("/login")
public class LoginRest {
	
	@Autowired
	private LoginDao loginDao;

	@PostMapping
	public Map<String, Object> post(@RequestBody Map<String, String> body) {
		
		
		String usuario = body.get("usuario");
		String senha = body.get("senha");
		
		Token token = loginDao.getToken(usuario, senha);

		System.out.println(usuario);
		System.out.println(senha);
		
		Map<String, Object> result = new HashMap<>();
		result.put("access_token", token.getAccessToken());
		return result;
	}
	
}
